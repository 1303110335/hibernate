package com.itheima.domain;//import org.junit.Test;

import com.itheima.utils.HibernateUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * hibernate CRUD操作
 */
public class HibernateDemo3 {
    @Test
    public void testSave() {
//        Customer c = new Customer();
//        c.setCustName("second");
        LinkMan c = new LinkMan();
        c.setLkmName("first");
        Session s = HibernateUtils.openSession();
        Transaction tx = s.beginTransaction();
        s.save(c);
        tx.commit();
        s.close();
    }

    @Test
    public void testFindOne() {
        Session s = HibernateUtils.openSession();
        //查询id为5的用户
        Customer c = s.get(Customer.class, 5L);
        System.out.println(c);
        s.close();
    }

    @Test
    public void testUpdate() {
        Session s = HibernateUtils.openSession();
        Transaction tx = s.beginTransaction();
        //查询id为5的用户
        Customer c = s.get(Customer.class, 5L);
        //修改客户的地址为：北京市顺义区
        c.setCustAddress("北京市顺义区");
        s.update(c);
        System.out.println(c);
        tx.commit();
        s.close();
    }

    @Test
    public void testDelete() {
        Session s = HibernateUtils.openSession();
        Transaction tx = s.beginTransaction();
        //查询id为5的用户
        Customer c = s.get(Customer.class, 5L);
        s.delete(c);
        System.out.println(c);
        tx.commit();
        s.close();
    }

    @Test
    public void testFindAll() {
        Session s = HibernateUtils.openSession();
        SQLQuery sqlQuery = s.createSQLQuery("select * from cst_customer");
        List<Object[]> list = sqlQuery.list();
        for (Object[] os : list) {
            for (Object b : os)
                System.out.println(b);
        }

        s.close();
    }
}
