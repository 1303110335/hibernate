package com.itheima.domain;

import com.itheima.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import org.hibernate.query.Query;
import java.util.List;

/**
 * hibernate中的HQL查询
 * 设计的对象
 *  Query
 * 如何获取该对象
 *  session.createQuery(String hql)
 *  是把sql语句的表名换成类名，把字段名换成实体中的字段名
 */
public class HibernateDemo6 {
    //基本查询
    @Test
    public void test1() {
        Session s = HibernateUtils.getCurrentSession();
        Transaction tx = s.beginTransaction();
        Query query = s.createQuery("from Customer where custLevel = ?1 and custName like ?2");
        query.setString(1, "21");
        query.setString(2, "%黑%");
        List list = query.list();
        for (Object o : list)
            System.out.println(o);
        tx.commit();
    }

    @Test
    public void test2() {
        Session s = HibernateUtils.getCurrentSession();
        Transaction tx = s.beginTransaction();
        Query query = s.createQuery("from Customer where custLevel = :custLevel and custName like :custName");
        query.setString("custLevel", "21");
        query.setString("custName", "%黑%");
        List list = query.list();
        for (Object o : list)
            System.out.println(o);
        tx.commit();
    }
    //条件查询
    //排序查询
    //分页查询
    @Test
    public void test3() {
        Session s = HibernateUtils.getCurrentSession();
        Transaction tx = s.beginTransaction();
        Query query = s.createQuery("from Customer");
        query.setFirstResult(2);
        query.setMaxResults(2);
        List list = query.list();
        for (Object o : list)
            System.out.println(o);
        tx.commit();
    }

    /**
     * 统计查询
     * 聚合函数：count sum max min avg
     */
    @Test
    public void test4() {
        Session s = HibernateUtils.getCurrentSession();
        Transaction tx = s.beginTransaction();
        Query query = s.createQuery("select count(*) from Customer");
        Long count = (Long) query.uniqueResult();//当返回的结果唯一时，可以使用，如果返回的结果不唯一，会抛出异常
        System.out.println(count);

        tx.commit();
    }

    /**
     * 投影查询:当我们在查询实体时，只需要部分字段，而不是全部，并且希望放回的结果使用实体类来封装，而不是
     * Object数组，这个时候我们称之为创建实体类的投影
     *
     * 投影查询的用法：
     *      1.查询语句
     */
    @Test
    public void test6() {
        Session s = HibernateUtils.getCurrentSession();
        Transaction tx = s.beginTransaction();
        Query query = s.createQuery("select new com.itheima.domain.Customer(custId,custName) from Customer ");
        List<Customer> list = query.list();
        System.out.println(list);
        tx.commit();
    }

    @Test
    public void test5() {
        Session s = HibernateUtils.getCurrentSession();
        Transaction tx = s.beginTransaction();
        Query query = s.createQuery("select custId,custName from Customer ");
        List<Object[]> list = query.list();
        for (Object[] os : list) {
            for (Object o : os) {
                System.out.println(o);
            }
        }

        tx.commit();
    }
}
