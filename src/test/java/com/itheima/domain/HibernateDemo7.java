package com.itheima.domain;

import com.itheima.utils.HibernateUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.junit.Test;

import java.util.List;

/**
 * hibernate中的HQL查询
 * 设计的对象
 *  Query
 * 如何获取该对象
 *  session.createQuery(String hql)
 *  是把sql语句的表名换成类名，把字段名换成实体中的字段名
 */
public class HibernateDemo7 {
    //基本查询
    @Test
    public void test1() {
        Session s = HibernateUtils.getCurrentSession();
        Transaction tx = s.beginTransaction();
        Criteria c = s.createCriteria(Customer.class);
//        c.setFirstResult()
//        c.setMaxResults()
//        c.add(Restrictions.eq("key", "val"));
//        c.setProjection(Projections.count())
//        c.setProjection(Projections.rowCount())
        List list = c.list();
        for (Object o : list)
            System.out.println(o);
        tx.commit();
    }

}


