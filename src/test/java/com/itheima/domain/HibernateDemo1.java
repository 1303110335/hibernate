package com.itheima.domain;//import org.junit.Test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import java.io.*;

/**
 * hibernate的入门案例
 */
public class HibernateDemo1 {
    @Test
    /**
     * 步骤分析
     * 1.解析主配置文件 Configuration
     * 2.根据主配置文件创建SessionFactory 重要原则：一个应用应该只有一个SessionFactory,在应用加载时创建，卸载时销毁。
     *      sessionFactory维护的信息
     *          连接数据库的信息
     *          hibernate的基本配置
     *          映射文件的位置，以及映射文件中的配置
     *          一些预定义的sql语句，（这些语句都是通用的）
     *              比如：全字段保存，根据id查询全字段信息
     *          hibernate的二级缓存
     *
     * 3.根据SessionFactory创建session  很重要：它是负责操作数据库
     *          使用原则：一个线程只能有一个
     *          是一个轻量级对象，且不是线程安全的
     *          它维护了hibernate的一级缓存
     *          它的反复创建不会消耗太多资源
     * 4.开始事务
     * 5.执行操作
     * 6.提交事务
     * 7.释放资源
     */
    public void test1() throws IOException {

        Customer c = new Customer();
        c.setCustName("first");

        //1.解析主配置文件
        Configuration cfg = new Configuration();
        cfg.configure();//hibernate.cfg.xml
        //2.根据主配置文件创建SessionFactory
        SessionFactory factory = cfg.buildSessionFactory();
        //3.根据SessionFactory创建session
        Session session = factory.openSession();
        //4.开始事务
        Transaction tx = session.beginTransaction();
        //5.执行操作
        session.save(c);
        //6.提交事务
        tx.commit();
        //7.释放资源
        session.close();
        factory.close();
    }

    public static java.lang.String read(java.lang.String filename) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(filename));
        java.lang.String s;
        StringBuilder sb = new StringBuilder();
        while((s = in.readLine()) != null){
            sb.append(s + "\n");
        }
        in.close();
        return sb.toString();
    }
}
