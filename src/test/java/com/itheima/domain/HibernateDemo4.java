package com.itheima.domain;//import org.junit.Test;

import com.itheima.utils.HibernateUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.jdbc.Work;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * hibernate CRUD操作
 */
public class HibernateDemo4 {
    @Test
    /**
     * hibernate中如何使用原始jdbc api
     * JDBC的api
     *      Connection
     *      Statement
     *      PreparedStatement
     *      ResultSet
     */
    public void test1() {
        Session session = HibernateUtils.openSession();
        session.doWork(new Work() {
            public void execute(Connection conn) throws SQLException {
                System.out.println(conn.getClass().getName());
            }
        });
    }
}
