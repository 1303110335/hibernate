package com.itheima.domain;

import com.itheima.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

public class Hibernate2 {

    @Test
    public void test1() {
        Session session = HibernateUtils.openSession();
        Customer c1 = session.get(Customer.class, 1L);//先去数据库中查询，并将查询存入了一级缓存中
        System.out.println(c1);
        Customer c2 = session.get(Customer.class, 1L);//先去一级缓存中查询，再去数据库中查询
        System.out.println(c2);
        System.out.println(c1 == c2);
        session.close();//serssion关闭，一级缓存就消失了
    }

    @Test
    public void test2() {
        Session session = HibernateUtils.openSession();
        Transaction tx = session.beginTransaction();
        Customer c1 = session.get(Customer.class, 5L);//先去数据库中查询，并将查询存入了一级缓存中

        c1.setCustAddress("浙江省");
        tx.commit();
        session.close();//serssion关闭，一级缓存就消失了

        c1.setCustAddress("浙江省1");//托管状态

    }

    @Test
    /**
     * 当我们把session和线程绑定之后，hibernate就会在提交或者回滚之后，自动帮我们关闭session
     */
    public void test3() {
        Session s1 = HibernateUtils.getCurrentSession();
        Session s2 = HibernateUtils.getCurrentSession();
        System.out.println(s1 == s2);
//        s1.close();
//        s2.close();
    }
}
