package com.itheima.domain;//import org.junit.Test;

import com.itheima.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * hibernate 中查询的一个方法
 * get (Class clazz, Serializable id)    立即加载  返回实体类类型
 * load (Class clazz, Serializable id)  延迟加载  返回实体类类型的代理对象
 *      load方法默认情况下是延迟，可以通过配置的方式改为立即加载
 */
public class HibernateDemo5 {
    @Test
    public void test1() {
        Session session = HibernateUtils.openSession();
        Customer c1 = session.get(Customer.class, 4L);
        System.out.println(c1.toString());

    }

    @Test
    public void test2() {
        Session session = HibernateUtils.openSession();
        Customer c2 = session.load(Customer.class, 4L);
        System.out.println(c2.toString());

    }
}
